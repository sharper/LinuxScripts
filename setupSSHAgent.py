#!/usr/bin/env python3

"""
A simple script to setup the environment variables for ssh-agent.

It will deduce if one is running and if not start one
Designed for RAL setup, many servers sharing same home directory
The ssh agent bind address will be ssh-$HOSTNAME-date_time/agent.PID

intended usage:
eval "$(~/setupSSHAgent.py)"


"""

import os 
import subprocess
import sys
import re

def get_active_agents():
    """
    Gets list of active ssh-agents for current user using ps
    """
    out,err = subprocess.Popen(["ps","-u",os.getenv("USER")],
                               stdout=subprocess.PIPE,universal_newlines=True).communicate()
    agents=[]
    for line in out.splitlines():
        if line.split()[3]=="ssh-agent":
            agents.append(line.split()[0])    

    return agents

def get_bind_addresses(base_dir):
    """
    Searches a specified directory for ssh-agent bind addresses.
    
    format is base_dir/ssh-$HOSTNAME-data_time/agent.PID
    """
    import glob
    addresses = glob.glob(base_dir + "/ssh-" + os.getenv("HOSTNAME") + "-*/agent.*")
    addresses.sort()
    return addresses;
    
def rm_inactive_bind_addresses(bind_addresses,active_agents):
    """ 
    Checks if the bind addresses are inactive.

    This looks at the list of found bind_addresses for this host
    and compares the PIDs of them to the list of active agents.
    Any inactive bind_addreses are removed.
    A new list is returned with only active bind addresses
    """
    active_addresses = []
    for bind_address in bind_addresses:
        pid = bind_address.split(".")[-1]
        
        if pid not in active_agents:
            os.remove(bind_address)
            bind_address_dir = bind_address.replace("/agent." + pid,"/")
            try:
                os.rmdir(bind_address_dir)
            except OSError as e:
                print(f"echo error removing {bind_address_dir}")
                pass
        else:
            active_addresses.append(bind_address)
    return active_addresses


def make_new_agent(base_dir):
    """
    makes a new ssh-agent
    
    bind_address is base_dir/ssh-$HOSTNAME-data_time/agent.PID
    """
   
    import datetime
    curr_time = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    
    bind_address_dir = (base_dir + "/ssh-" + os.getenv("HOSTNAME") + 
                        "-" + curr_time)
    os.makedirs(bind_address_dir)

    bind_address_tmp = bind_address_dir + "/agent.tmp"
    
    out,err = subprocess.Popen(["ssh-agent","-a",bind_address_tmp,"-s"],
                               stdout=subprocess.PIPE,universal_newlines=True).communicate()
    for line in out.splitlines():
        if line.find("echo Agent pid")==0:
            agent_pid = line.split()[3][:-1]
            bind_address = bind_address_dir + "/agent." +  agent_pid
            os.rename(bind_address_tmp,bind_address)      
            out = out.replace(bind_address_tmp,bind_address)

    print(out)
                       
    
host_name = os.getenv("HOSTNAME")
try:
    if re.search(r'^mercury(\d+).pp.rl.ac.uk\Z',host_name)==None and re.search(r'^heplnx(\d+).pp.rl.ac.uk\Z',host_name)==None:
        print("echo host",host_name," is not in mercury or heplnx .pp.rl.ac.uk")
        import sys
        sys.exit()
except:
    print("echo host name issue",host_name)
    sys.exit()
        
base_bind_address_dir=os.getenv("HOME") + "/.sshagentdata"
if not os.path.exists(base_bind_address_dir):
    os.makedirs(base_bind_address_dir)

active_agents=get_active_agents()
bind_addresses=get_bind_addresses(base_bind_address_dir)
bind_addresses=rm_inactive_bind_addresses(bind_addresses,active_agents)

if not bind_addresses:
    make_new_agent(base_bind_address_dir)
    print("ssh-add ~/.ssh/id_rsa")
else:
    bind_address = bind_addresses[-1]
    pid = bind_address.split(".")[-1]
    
    print("SSH_AUTH_SOCK="+bind_address+"; export SSH_AUTH_SOCK;")
    print("SSH_AGENT_PID="+pid+"; export SSH_AGENT_PID;")
    print("echo reusing Agent pid "+pid+";")
    
